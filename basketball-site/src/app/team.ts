import { TeamPlayer } from './team-player';

export interface Team {
    team_id:number;
    team_name:string;
    coach:string;
    team_avg:number;
    players:TeamPlayer[];
    
}