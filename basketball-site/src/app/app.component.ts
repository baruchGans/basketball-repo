import { Component } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Basketball League site';

  constructor(private authService:AuthService,
     private router: Router,
     private location: Location) {
   }

   hasToken(){
    return localStorage.getItem('token');
   }


  logout(){
    this.authService.logout()
    this.router.navigate(['login']);
  }

  goBack(): void {
    this.location.back();
  }
}
