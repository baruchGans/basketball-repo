import { Player } from './player';

export const PLAYER: Player = {
    "player_name": "ברוך  גנס",
    "player_height": 1.55,
    "amount_of_games": 3,
    "avg_points": 14.7
}