import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-login',
  template: `
  <div style="text-align:center">
    <h1>
      Login
    </h1>
 

  <mat-form-field class="example-form-field">
  <mat-label>username</mat-label>
  <input matInput type="text" [(ngModel)]="username">
  <button mat-button *ngIf="username" matSuffix mat-icon-button aria-label="Clear" (click)="username=''">
    <mat-icon>close</mat-icon>
  </button>
</mat-form-field>
<br>
<mat-form-field class="example-form-field">
<mat-label>password</mat-label>
<input matInput type="text" [(ngModel)]="password">
<button mat-button *ngIf="password" matSuffix mat-icon-button aria-label="Clear" (click)="password=''">
  <mat-icon>close</mat-icon>
</button>
</mat-form-field>
<br>
  <button mat-raised-button color (click)="login()">login</button>
  </div>
  `
})
export class LoginComponent implements OnInit {

    error: any;
    username: string;
    password: string;
    constructor(
      private authService: AuthService,
      private router: Router,
      
    ) { }
  
    ngOnInit() {
    }
  
    login() {
      this.authService.login(this.username, this.password).subscribe(
        success => this.router.navigate(['dashboard']),
        error => this.error = error
      );
    }
  }