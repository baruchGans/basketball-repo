import { Game } from './game';

export const GAMES: Game = {
  "EI": [
      {
          "game_stage": "Eighth",
          "teamA": {
              "name": "macabi tel aviv",
              "id": 2,
              "points": 23
          },
          "teamB": {
              "name": "הפועל תל אביב",
              "id": 3,
              "points": 48
          }
      },
      {
        "game_stage": "Eighth",
        "teamA": {
            "name": "macabi tel aviv",
            "id": 2,
            "points": 23
        },
        "teamB": {
            "name": "הפועל תל אביב",
            "id": 3,
            "points": 48
        }
    },
    {
      "game_stage": "Eighth",
      "teamA": {
          "name": "macabi tel aviv",
          "id": 2,
          "points": 23
      },
      "teamB": {
          "name": "הפועל תל אביב",
          "id": 3,
          "points": 48
      }
  },
  {
    "game_stage": "Eighth",
    "teamA": {
        "name": "macabi tel aviv",
        "id": 2,
        "points": 23
    },
    "teamB": {
        "name": "הפועל תל אביב",
        "id": 3,
        "points": 48
    }
},
{
  "game_stage": "Eighth",
  "teamA": {
      "name": "macabi tel aviv",
      "id": 2,
      "points": 23
  },
  "teamB": {
      "name": "הפועל תל אביב",
      "id": 3,
      "points": 48
  }
},
{
  "game_stage": "Eighth",
  "teamA": {
      "name": "macabi tel aviv",
      "id": 2,
      "points": 23
  },
  "teamB": {
      "name": "הפועל תל אביב",
      "id": 3,
      "points": 48
  }
},
{
  "game_stage": "Eighth",
  "teamA": {
      "name": "macabi tel aviv",
      "id": 2,
      "points": 23
  },
  "teamB": {
      "name": "הפועל תל אביב",
      "id": 3,
      "points": 48
  }
},
      {
          "game_stage": "Eighth",
          "teamA": {
              "name": "macabi tel aviv",
              "id": 2,
              "points": 48
          },
          "teamB": {
              "name": "הפועל תל אביב",
              "id": 3,
              "points": 48
          }
      }
  ],
  "QU": [
      {
          "game_stage": "Quarter",
          "teamA": {
              "name": "macabi tel aviv",
              "id": 2,
              "points": 48
          },
          "teamB": {
              "name": "הפועל תל אביב",
              "id": 3,
              "points": 45
          }
      },
      {
        "game_stage": "Quarter",
        "teamA": {
            "name": "macabi tel aviv",
            "id": 2,
            "points": 48
        },
        "teamB": {
            "name": "הפועל תל אביב",
            "id": 3,
            "points": 45
        }
    },
    {
      "game_stage": "Quarter",
      "teamA": {
          "name": "macabi tel aviv",
          "id": 2,
          "points": 48
      },
      "teamB": {
          "name": "הפועל תל אביב",
          "id": 3,
          "points": 45
      }
  },
  {
    "game_stage": "Quarter",
    "teamA": {
        "name": "macabi tel aviv",
        "id": 2,
        "points": 48
    },
    "teamB": {
        "name": "הפועל תל אביב",
        "id": 3,
        "points": 45
    }
},
  ],
  "SF": [
      {
          "game_stage": "Semifinals",
          "teamA": {
              "name": "הפועל תל אביב",
              "id": 3,
              "points": 30
          },
          "teamB": {
              "name": "macabi tel aviv",
              "id": 2,
              "points": 48
          }
      },
      {
          "game_stage": "Semifinals",
          "teamA": {
              "name": "הפועל רעננה",
              "id": 4,
              "points": 22
          },
          "teamB": {
              "name": "macabi tel aviv",
              "id": 2,
              "points": 48
          }
      }
  ],
  "FI": [
      {
          "game_stage": "Final",
          "teamA": {
              "name": "macabi tel aviv",
              "id": 2,
              "points": 48
          },
          "teamB": {
              "name": "הפועל תל אביב",
              "id": 3,
              "points": 22
          }
      }
  ]
}