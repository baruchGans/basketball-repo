export interface OneGame{
    game_stage: string;
    teamA: {
        name:string;
        id:number;
        points:number;
    }
    teamB: {
        name:string;
        id:number;
        points:number;
    }
    
}