import { Injectable } from '@angular/core';
import { Team } from './team';
import { TEAM } from './mock-team';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_ROOT } from './constants';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  private teamrUrl = API_ROOT +'/league/team_details/';  // URL to web api


  constructor(private http: HttpClient) { }


  getTeam(id: number): Observable<Team>{
    let url = this.teamrUrl + "?team_id=" + id
    return this.http.get<Team>(url)
  }
}
