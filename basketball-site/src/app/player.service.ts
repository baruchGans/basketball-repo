import { Injectable } from '@angular/core';
import { Player } from './player';
import { PLAYER } from './mock-player';
import { Observable} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_ROOT } from './constants';


@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  private playerUrl = API_ROOT + '/league/player_details/';  // URL to web api


  constructor(private http: HttpClient) { }

  getPlayer(id: number): Observable<Player>{
    let url = this.playerUrl + "?player_id=" + id
    return this.http.get<Player>(url)
  }
}
