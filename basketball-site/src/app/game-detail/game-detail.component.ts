import { Component, OnInit, Input } from '@angular/core';
import { OneGame } from '../one-game';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css']
})
export class GameDetailComponent implements OnInit {

  @Input() game: OneGame;
  @Input() user_details;




  
 
  constructor() { }

  ngOnInit(): void {
    
  }



 isAdminOrCoach(team_id:number):boolean{
  let user_type = this.user_details.user_type
  if(user_type == "leagueadmin")return true;
  else if(user_type == "player")return false;
  else if(user_type == "coach"){
    return this.user_details.team_id == team_id;
  }
};

}
