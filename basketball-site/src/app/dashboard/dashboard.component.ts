import { Component, OnInit } from '@angular/core';
import { Game } from '../game';
import { GameService } from '../game.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  games :Game;
  user_details;


  constructor(private gameService: GameService,
    private authService: AuthService,) { }

  ngOnInit(): void {
    this.getGames()
    this.getUserDetails()

  }

  getGames(): void {
     this.gameService.getdashboardGames()
    .subscribe(games => 
      this.games = games
      );
  }

  getUserDetails(): void {
    this.authService.getUserDetails()
   .subscribe(res => this.user_details = res);
   
 }





  


}
