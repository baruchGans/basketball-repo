import { Injectable } from '@angular/core';
import { Game } from './game';
import { GAMES } from './mock-games';
import { Observable} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_ROOT } from './constants';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private dashboardUrl = API_ROOT +'/league/competition_dashboard/'; 
  constructor( private http: HttpClient,
    ) { }


  getdashboardGames(): Observable<Game>{
    return this.http.get<Game>(this.dashboardUrl)
  }

}
