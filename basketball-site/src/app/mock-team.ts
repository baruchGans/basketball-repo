import { Team } from './team';

export const TEAM: Team = {
    "team_id": 3,
    "team_name": "הפועל תל אביב",
    "coach": "fdf  גנס",
    "players": [
        {
            "id": 3,
            "first_name": "ברוך",
            "last_name": "גנס"
        },
        {
            "id": 4,
            "first_name": "we45yhwe45y",
            "last_name": "serysert"
        },
        {
            "id": 6,
            "first_name": "ברוך המלך",
            "last_name": "אדמין dfbxdfb"
        }
    ],
    "team_avg": 32.3
}