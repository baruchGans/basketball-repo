
export interface Player {
    player_name:string;
    player_height:number;
    amount_of_games:number;
    avg_points:number;
    
}