# README #


An app that simulates a Basketball league competition site.

with eighth, quarterfinals, semifinals and finals.

There are three types of users on the site, with different permission levels, as follows:

Player: Can see the scoreboard and the final score for each team in each game.

Coach: Can see what a player can see. Plus his group page. The details for each player.


League manager: Can see all details for all teams and players.

## Getting Started


### Prerequisites


```
Python 3.8.3
Django 3.0.8
Angular: 9.1.11
```

### Installing

to clone the project from git:
```
git clone https://bitbucket.org/baruchGans/basketball-repo.git
```

use:

```
pip3 freeze > requirements.txt
```

 to get and install all dependencies.

To create fake data with all data, use the command:

```
python manage.py generate_fake_data
```

To create a league manager who can see all the data, use the command:
```
python manage.py generate_league_admin
```



