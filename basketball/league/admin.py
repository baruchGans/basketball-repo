from django.contrib import admin

from .models import LeagueAdmin, Coach, Team, Player, Game, PlayerScore

admin.site.register(LeagueAdmin)
admin.site.register(Coach)
admin.site.register(Team)
admin.site.register(Player)
admin.site.register(Game)
admin.site.register(PlayerScore)
