from django.http import HttpResponseForbidden
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.utils import timezone
import random
import string
from league.models import LEAGUE_ADMIN, COACH,PLAYER
from basketball.constants import PERMISSION_LEVEL_ADMIN, PERMISSION_LEVEL_COACH


def is_api_allowed(permission_level_requested):
    def decorator(function):
        def wrapper(*args, **kwargs):
            request_user = args[0].user
            api_allowed = True
            user_type = check_if_user_is_leagueadmin_coach_or_player(request_user)
            if permission_level_requested == PERMISSION_LEVEL_ADMIN:
                api_allowed = user_type == LEAGUE_ADMIN
            if permission_level_requested == PERMISSION_LEVEL_COACH:
                api_allowed = user_type in [COACH, LEAGUE_ADMIN]

            if not api_allowed:
                return HttpResponseForbidden()
            result = function(*args, **kwargs)
            return result
        return wrapper
    return decorator


def check_if_user_is_leagueadmin_coach_or_player(user):
    if getattr(user, LEAGUE_ADMIN, False):
        return LEAGUE_ADMIN

    if getattr(user, COACH, False):
        return COACH

    if getattr(user, PLAYER, False):
        return PLAYER

    return False


def get_all_logged_in_users():
    # Query all non-expired sessions
    # use timezone.now() instead of datetime.now() in latest versions of Django
    sessions = Session.objects.filter(expire_date__gte=timezone.now())
    uid_list = []

    # Build a list of user ids from that query
    for session in sessions:
        data = session.get_decoded()
        uid_list.append(data.get('_auth_user_id', None))

    # Query all logged in users based on id list
    return User.objects.filter(id__in=uid_list)

def random_string(stringLength=8):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


def random_height():
    return round(random.uniform(1.8, 2.3), 2)