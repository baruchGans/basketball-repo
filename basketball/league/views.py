from django.contrib.auth.models import User
from django.db.models import Sum, Avg
from django.http import HttpResponseForbidden, HttpResponse, JsonResponse
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import api_view
from league.models import Team, Player, PlayerScore, Game, COACH, LEAGUE_ADMIN, PLAYER
from .utils import is_api_allowed, PERMISSION_LEVEL_COACH, PERMISSION_LEVEL_ADMIN, \
    check_if_user_is_leagueadmin_coach_or_player, get_all_logged_in_users


@api_view(['GET'])
def competition_dashboard(request):
    stages_dict = {Game.EIGHTH: [], Game.QUARTER: [], Game.SEMIFINALS: [], Game.FINAL: []}

    for game in Game.objects.all():
        game_obj = {"game_stage": game.get_game_stage_display(), "teamA": {}, "teamB": {}}
        game_obj["teamA"]["name"] = game.teamA.name
        game_obj["teamA"]["id"] = game.teamA.id
        game_obj["teamA"]["points"] = \
            PlayerScore.objects.filter(player__player_team=game.teamA, game=game) \
                .aggregate(total_points=Sum("points"))["total_points"]

        game_obj["teamB"]["name"] = game.teamB.name
        game_obj["teamB"]["id"] = game.teamB.id
        game_obj["teamB"]["points"] = \
            PlayerScore.objects.filter(player__player_team=game.teamB, game=game) \
                .aggregate(total_points=Sum("points"))["total_points"]

        stages_dict[game.game_stage].append(game_obj)

    return JsonResponse(stages_dict, safe=False)


@api_view(['GET'])
@is_api_allowed(PERMISSION_LEVEL_COACH)
def team_details(request):
    team_id = request.GET.get("team_id", "Empty")

    if not team_id.isdigit():
        return JsonResponse({'message': 'missing or Illegal team_id'}, status=400)

    team_queryset = Team.objects.filter(id=team_id)
    if check_if_user_is_leagueadmin_coach_or_player(request.user) == COACH:
        team_queryset = team_queryset.filter(coach=request.user.coach)

    team = team_queryset.first()

    if team:
        players = list(Player.objects.filter(player_team=team).values('id', 'first_name', 'last_name'))
        team_points_per_game = list(PlayerScore.objects.filter(player__player_team=team) \
                                    .values('game') \
                                    .annotate(sum_points=Sum('points')))

        avg_team_points = sum(game["sum_points"] for game in team_points_per_game) / len(team_points_per_game)

        data = {"team_id": team.id,
                "team_name": team.name,
                "coach": team.coach.full_name,
                "players": players,
                "team_avg": round(avg_team_points, 1) if avg_team_points else 0

                }
        return JsonResponse(data, safe=False)

    else:
        return HttpResponse(status=404)


@api_view(['GET'])
@is_api_allowed(PERMISSION_LEVEL_COACH)
def player_details(request):
    player_id = request.GET.get("player_id", "Empty")
    if not player_id.isdigit():
        return JsonResponse({'message': 'missing or Illegal player_id'}, status=400)

    player_queryset = Player.objects.filter(id=player_id)
    if check_if_user_is_leagueadmin_coach_or_player(request.user) == COACH:
        player_queryset = player_queryset.filter( player_team__coach=request.user.coach)
    player = player_queryset.first()

    if player:
        player_games = PlayerScore.objects.filter(player=player)
        amount_of_games = player_games.count()
        avg_points = player_games.aggregate(Avg('points')).get("points__avg")
        data = {"player_name": player.full_name,
                "player_height": player.height,
                "amount_of_games": amount_of_games,
                "avg_points": round(avg_points, 1) if avg_points else 0
                }
        return JsonResponse(data, safe=False)

    else:
        return HttpResponse(status=404)


@api_view(['GET'])
def user_types(request):
    user_type = check_if_user_is_leagueadmin_coach_or_player(request.user)
    data = {"user_type":user_type}
    if user_type == COACH:
        data["team_id"] = getattr(request.user, COACH).team.id

    return JsonResponse(data, safe=False)


@api_view(['GET'])
@is_api_allowed(PERMISSION_LEVEL_ADMIN)
def logged_in_users(request):
    all_users_logged_in = []
    all_online_users = get_all_logged_in_users()
    for user in all_online_users:
        user_related_type = check_if_user_is_leagueadmin_coach_or_player(user)
        if user_related_type:
            related_basketball_user = getattr(user, user_related_type)
            all_users_logged_in.append(related_basketball_user)
    return HttpResponse(all_users_logged_in)

