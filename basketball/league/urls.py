from rest_framework.routers import DefaultRouter
from django.conf.urls import url
from .views import *

router = DefaultRouter()
urlpatterns = router.urls
urlpatterns.append(url(r'competition_dashboard/', competition_dashboard))
urlpatterns.append(url(r'team_details/', team_details))
urlpatterns.append(url(r'player_details/', player_details))
urlpatterns.append(url(r'user_types/', user_types))
urlpatterns.append(url(r'logged_in_users/', logged_in_users))
