from django.contrib.auth.models import User
from django.core.management import BaseCommand
from faker import Faker

from league.models import LeagueAdmin

faker = Faker()


class Command(BaseCommand):
    def handle(self, *args, **options):
        username = input("Please enter username:\n")
        password = input("Please enter password:\n")

        league_admin_user = User.objects.create_user(username, password=password)
        team_coach = LeagueAdmin.objects.create(user=league_admin_user, first_name=faker.first_name(),
                                                last_name=faker.last_name())

        self.stdout.write(self.style.SUCCESS('Successfully created league admin'))
