import random

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.db.models import Sum
from league.models import Coach, Team, Player, Game, PlayerScore
from league.utils import random_string, random_height
from faker import Faker

faker = Faker()

teams = [
    "Boston Celtics", "New Jersey Nets", "New York Knicks", "Toronto Raptors", "Chicago Bulls",
    "Cleveland Cavaliers", "Detroit Pistons", "Indiana Pacers", "Milwaukee Bucks", "Atlanta Hawks",
    "Charlotte Bobcats", "Miami Heat", "Orlando Magic", "Washington Wizards", "Los Angeles Lakers",
    "Los Angeles Clippers"
]


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Eighth-finals
        for i in range(8):
            create_two_teams_and_create_a_game_between_them()

        # Find the eight wins that go into the quarterfinals
        create_all_stage_games(Game.EIGHTH, 8, Game.QUARTER)
        # The semi-final
        create_all_stage_games(Game.QUARTER, 4, Game.SEMIFINALS)
        # Grand Final
        create_all_stage_games(Game.SEMIFINALS, 2, Game.FINAL)

        self.stdout.write(self.style.SUCCESS('Successfully created all fake data'))


def create_team_with_players():
    # create coach
    coach_user = User.objects.create_user(random_string(), password=random_string())
    team_coach = Coach.objects.create(user=coach_user, first_name=faker.first_name(), last_name=faker.last_name())

    # create team
    # get random team from teams list and pop it from list
    team_name = teams.pop(random.randrange(len(teams)))
    team = Team.objects.create(name=team_name, coach=team_coach)

    # create 10 players
    for i in range(10):
        player_user = User.objects.create_user(random_string(), password=random_string())
        Player.objects.create(user=player_user, height=random_height(), player_team=team, first_name=faker.first_name(),
                              last_name=faker.last_name())
    return team


def set_results_for_team_in_the_game(game, teamA):
    teamA_players = list(teamA.player_set.all())
    number_of_players_shared_in_the_game = random.randint(5, 10)
    # Select random players to share in the game
    random_players = random.sample(teamA_players, number_of_players_shared_in_the_game)
    for player in random_players:
        # Determine a random number of points a player scored in a basketball game
        PlayerScore.objects.create(player=player, game=game, points=random.randint(1, 30))


def create_two_teams_and_create_a_game_between_them():
    teamA = create_team_with_players()
    teamB = create_team_with_players()
    create_game(teamA, teamB, Game.EIGHTH)


def create_game(teamA, teamB, game_stage):
    game = Game.objects.create(game_stage=game_stage, teamA=teamA, teamB=teamB)
    set_results_for_team_in_the_game(game, teamA)
    set_results_for_team_in_the_game(game, teamB)


def create_all_stage_games(previous_stage, winners_quantity, current_stage):
    all_team_ids_and_points = list(PlayerScore.objects.filter(game__game_stage=previous_stage)
                                   .values('player__player_team')
                                   .annotate(sum_points=Sum('points')))
    four_winners_ids_and_points = sorted(all_team_ids_and_points, key=lambda x: x["sum_points"], reverse=True)[
                                  0:winners_quantity]
    list_four_ids = map(lambda x: x["player__player_team"], four_winners_ids_and_points)
    four_winners = Team.objects.filter(id__in=list_four_ids)
    for pair in zip(four_winners[::2], four_winners[1::2]):
        game = Game.objects.create(game_stage=current_stage, teamA=pair[0], teamB=pair[1])
        set_results_for_team_in_the_game(game, pair[0])
        set_results_for_team_in_the_game(game, pair[1])
