from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import CASCADE

from basketball.constants import NUMBER_OF_TEAMS_PARTICIPATING, NUMBER_OF_PLAYERS_IN_THE_TEAM


class BasketballUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    class Meta:
        abstract = True
        # unique_together = ('first_name', 'last_name')

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

    @property
    def full_name(self):
        return "{}  {}".format(self.first_name, self.last_name)


class LeagueAdmin(BasketballUser):
    email_address = models.EmailField(max_length=100, blank=True)

    def __str__(self):
        return "League Admin {}".format(super(LeagueAdmin, self).__str__())


class Coach(BasketballUser):
    phone_number = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return "Coach {}".format(super(Coach, self).__str__())


class Team(models.Model):
    name = models.CharField(max_length=30, unique=True)
    coach = models.OneToOneField(Coach, on_delete=CASCADE)

    def save(self, *args, **kwargs):

        if Team.objects.count() >= NUMBER_OF_TEAMS_PARTICIPATING:
            raise ValidationError("The number of teams in the league cannot be greater than 16")

        if Player.objects.filter(player_team=self).count() >= NUMBER_OF_PLAYERS_IN_THE_TEAM:
            raise ValidationError("A team cannot count more than ten players")

        return super(Team, self).save(*args, **kwargs)

    def __str__(self):
        return "{}".format(self.name)


class Player(BasketballUser):
    height = models.FloatField(max_length=3)
    player_team = models.ForeignKey(Team, on_delete=CASCADE)

    def __str__(self):
        return "Player {}".format(super(Player, self).__str__())


class Game(models.Model):
    EIGHTH = 'EI'
    QUARTER = 'QU'
    SEMIFINALS = 'SF'
    FINAL = 'FI'
    GAME_STAGES = [
        (EIGHTH, 'Eighth'),
        (QUARTER, 'Quarter'),
        (SEMIFINALS, 'Semifinals'),
        (FINAL, 'Final'),
    ]
    game_stage = models.CharField(
        max_length=2,
        choices=GAME_STAGES,
    )
    teamA = models.ForeignKey(Team, related_name='teamA', on_delete=CASCADE)
    teamB = models.ForeignKey(Team, related_name='teamB', on_delete=CASCADE)

    def save(self, *args, **kwargs):
        if self.teamA != self.teamB:
            return super(Game, self).save(*args, **kwargs)
        raise ValidationError("The two teams should be different")

    def __str__(self):
        return "{}: {} VS {}".format(self.get_game_stage_display(), self.teamA, self.teamB)


class PlayerScore(models.Model):
    player = models.ForeignKey(Player, on_delete=CASCADE)
    game = models.ForeignKey(Game, on_delete=CASCADE)
    points = models.IntegerField()

    def save(self, *args, **kwargs):
        if self.player.player_team in [self.game.teamA, self.game.teamB] :
            return super(PlayerScore, self).save(*args, **kwargs)
        raise ValidationError("The player must belong to one of the two teams")

    def __str__(self):
        return "{}. {}. {} points".format(self.game, self.player, self.points)

LEAGUE_ADMIN = LeagueAdmin.__name__.lower()
COACH = Coach.__name__.lower()
PLAYER = Player.__name__.lower()