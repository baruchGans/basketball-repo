from django.contrib import admin
from django.urls import include, path
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token


urlpatterns = [
    path('auth/login/', obtain_jwt_token),
    path('auth/refresh-token/', refresh_jwt_token),

    path('league/', include('league.urls')),
    path('admin/', admin.site.urls),
]